from Bio import AlignIO
from Bio.Align.Applications import ClustalOmegaCommandline
import subprocess
import os, glob
import classes


files = os.listdir(os.getcwd())
new_files = []

for x in files:
	if 'derep' in x:
		new_files.append(x)

print(new_files)


def deviation(num):

    return str(num/10).split('.')[0]


def self_comp(seq):


    with open('fasta.txt', 'w') as inf:
        inf.write('>' + 'primer' + '\n' + seq)


    bout = subprocess.call(['blastn', '-query', 'fasta.txt', '-subject', 'fasta.txt', '-outfmt', '6 std',
                            '-task', 'blastn', '-reward', '1', '-dust', 'no', '-penalty', '-3', '-gapopen', '5',
                            '-gapextend', '2', '-word_size', '4', '-out', 'out.txt'])

    with open('out.txt') as inf:
        all_file = inf.readlines()
        cnt = 0
        for line in all_file:
            cnt += 1

    os.remove('fasta.txt')
    os.remove('out.txt')

    return cnt


def inversComplement(input):
    output = ''
    for letter in input:
        letter = letter.upper()

        if letter == 'A':
            output += 'T'
        elif letter == 'T':
            output += 'A'
        elif letter == 'G':
            output += 'C'
        else:
            output += 'G'

    return output[::-1]


def mt(seq):
    G = seq.count('G')
    C = seq.count('C')
    T = seq.count('T')
    A = seq.count('A')
    if len(seq) > 14:
        tm = 64.9 + 41 * (G + C - 16.4)/(G + C + T + A)
    else:
        tm = (A + T) * 2 + (G + C) * 4
    return round(tm, 2)


def gc(seq):
    C = float(seq.count('C'))
    G = float(seq.count('G'))
    lenght = float(len(seq))
    GC = C+G
    return GC/lenght


def gc_clamp(seq):
    cnt = 0
    clamp = seq[-5:]
    g = clamp.count('G')
    c = clamp.count('C')

    return g+c


def end(seq):
    end = seq[-1:]
    if end == 'G':
        return 1
    elif end == 'C':
        return 1
    else:
        return 0


def runs(seq):
    if seq.find('AAAA') != -1:
        return 1
    elif seq.find('GGGG') != -1:
        return 1
    elif seq.find('CCCC') != -1:
        return 1
    elif seq.find('TTTT') != -1:
        return 1
    else:
        return 0

def profile(fasta):
    x = fasta.trim(fasta.freq())
    #print(x)
    strings = []

    for k, v in x.items():
        strings.append(v)
    #print(strings)
    default = [0] * len(strings[0])
    results = {
        'A': default[:],
        'C': default[:],
        'G': default[:],
        'T': default[:],
        '-': default[:],
    }
    for s in strings:
        for i, c in enumerate(s):
            try:
                results[c][i] += 1
            except KeyError:
                continue
    return results


def consensus(profile):
    result = []

    keys = ''

    for k, v in profile.items():
        keys = keys + k

    for i in range(len(profile[keys[0]])):
        # print(i)
        max_v = 0
        max_k = None
        for k in keys:
            v = profile[k][i]
            if v > max_v:
                max_v = v
                max_k = k
        result.append(max_k)

    return ''.join(result)


def logo_consensus(profile):

    coords = []
    keys = ''

    for k, v in profile.items():
        keys = keys + k

    for i in range(len(profile[keys[0]])):
        # print(i)
        max_v = 0
        max_k = None
        for k in keys:
            v = profile[k][i]
            if v > max_v:
                max_v = v
                max_k = k
        coords.append([max_v, max_k])

    return coords


def conserved(profile):

    result = []
    keys = ''
    default = ['-'] * len(profile['A'[0]])
    # print(default)
    for k, v in profile.items():
        keys = keys + k

    # print(keys)

    for i in range(len(profile[keys[0]])):
        for k in keys:
            v = profile[k][i]
            if v == fasta.count():
                default.pop(i)
                default.insert(i, k)
            else:
                continue


    print(''.join(default))


def msa(infile):

	name = infile.split('refs')[0]

	outfile = name + 'omegaout.txt'
	infile = infile

	prog_root = 'C:\\Users\Dexter2\Desktop\Home\PROGRAMS\clustal-omega-1.2.2-win64\clustal-omega-1.2.2-win64\clustalo' \
                '.exe'

	multiseqalign = ClustalOmegaCommandline(cmd=prog_root,
                                            infile=infile,
                                            outfile=outfile,
                                            verbose=True,
                                            outfmt='st',
                                            force=True)

	subprocess.call((str(multiseqalign)))


	with open(name + 'aligned.fasta', 'w') as outf:
		alignment = AlignIO.read(outfile, "stockholm")
		outf.write(alignment.format("fasta"))


	fasta = classes.Fasta(name + 'aligned.fasta')

	seq = consensus(profile(fasta)).replace('-', '')

	n = [16, 17, 18, 19, 20, 21, 22, 23, 24]
	primers = []
	for x in n:
		primers += [seq[i:i+x] for i in range(0, len(seq), 1)]

	print("Filter out primers...")

	forward = {}
	for pos in primers:
		if 19 <= len(pos) <= 23:
			if 0.5 < gc(pos) < 0.6:
				if 1 <= gc_clamp(pos) <= 2:
					if end(pos) == 1:
						if runs(pos) == 0:
							if 50 < mt(pos) < 60:
								#if self_comp(pos) == 1:
									forward[seq.find(pos) + len(pos)] = pos

	reverse = {}
	for pos in primers:
		if 19 <= len(pos) <= 23:
			if 0.5 < gc(pos) < 0.6:
				if 1 <= gc_clamp(inversComplement(pos)) <= 2:
					if end(inversComplement(pos)) == 1:
						if runs(pos) == 0:
							if 50 < mt(pos) < 60:
								#if self_comp(inversComplement(pos)) == 1:
									reverse[seq.find(pos)] = inversComplement(pos)


	# print(forward)
	# print(revese)

	print('Prepering summary of primer pairs...')

	with open(name + 'upv_temp.txt', 'w') as ouf:
		cnt = 0
		for for_pos, for_seq in forward.items():
			for rev_pos, rev_seq in reverse.items():
				if 100 < (rev_pos - for_pos) < 300:
					if abs(mt(for_seq)-mt(rev_seq)) < 0.001:
						print(abs(mt(for_seq)-mt(rev_seq)))
						cnt += 1
						ouf.write(name  + 'F_' + str(cnt) + '\t' + for_seq + '\t' +
								  name  + 'R_' + str(cnt) + '\t' + rev_seq + '\t' +
								  str(rev_pos - for_pos + (len(rev_seq) + len(for_seq))) + '\t' +
								  deviation(rev_pos - for_pos + (len(rev_seq) + len(for_seq))) + '\t' + '\t' +
							'/media/data/dbs/2020-01-13_UniPriVal_get_ref_seqs_extended_dbs/' + name[:-1] + '\t' +
							#	  '/home/agorecki/upv_ref_supplement/ref_db/' + name[:-1] + '\t' +
						 '/media/data/dbs/2020-01-13_UniPriVal_get_ref_seqs_extended_dbs/' + name[:-1] + '.txt' ' \t' +
						#	'/home/agorecki/upv_ref_supplement/ref_db' + name[:-1] + '.txt' ' \t' +
								  str(90) + '\t' + str(90) + '\n')


print('Done!...:)')



for file in new_files:
	msa(file)
